<?php


namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

use App\Http\Requests;
use App\Http\Controllers;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;




    public function insert(Request $request)
    {
        $fname = $request->input('fname');
        $pname = $request->input('pname');
        $typename = $request->input('tp');
        $group = $request->input('group');
        $file = $request->file('file');
        $fillename = $file->getClientOriginalName();

        // if ($typename == "WI" && $group == "CM") {
        //     $destinationPath = 'uploads/wi/cm';
        //     $file->move($destinationPath, $file->getClientOriginalName());
        // }

        // if ($typename == "WI" && $group == "MA") {
        //     $destinationPath = 'uploads/wi/ma';
        //     $file->move($destinationPath, $file->getClientOriginalName());
        // }

        // if ($typename == "WI" && $group == "ORG") {
        //     $destinationPath = 'uploads/wi/org';
        //     $file->move($destinationPath, $file->getClientOriginalName());
        // }

        // if ($typename == "WI" && $group == "PMC") {
        //     $destinationPath = 'uploads/wi/pmc';
        //     $file->move($destinationPath, $file->getClientOriginalName());
        // }

        // if ($typename == "WI" && $group == "PPQA") {
        //     $destinationPath = 'uploads/wi/ppqa';
        //     $file->move($destinationPath, $file->getClientOriginalName());
        // }

        // if ($typename == "WI" && $group == "PP") {
        //     $destinationPath = 'uploads/wi/pp';
        //     $file->move($destinationPath, $file->getClientOriginalName());
        // }

        // if ($typename == "WI" && $group == "REQM") {
        //     $destinationPath = 'uploads/wi/reqm';
        //     $file->move($destinationPath, $file->getClientOriginalName());
        // }

        // if ($typename == "WI" && $group == "SAM") {
        //     $destinationPath = 'uploads/wi/sam';
        //     $file->move($destinationPath, $file->getClientOriginalName());
        // }


        if ($typename == "UM" && $group == "CM") {
            $destinationPath = 'uploads/um/cm';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        if ($typename == "UM" && $group == "MA") {
            $destinationPath = 'uploads/um/ma';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        if ($typename == "UM" && $group == "ORG") {
            $destinationPath = 'uploads/um/org';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        if ($typename == "UM" && $group == "PMC") {
            $destinationPath = 'uploads/um/pmc';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        if ($typename == "UM" && $group == "PP") {
            $destinationPath = 'uploads/um/pp';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        if ($typename == "UM" && $group == "REQM") {
            $destinationPath = 'uploads/um/reqm';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        if ($typename == "UM" && $group == "SQA") {
            $destinationPath = 'uploads/um/sqa';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        if ($typename == "UM" && $group == "SCM") {
            $destinationPath = 'uploads/um/scm';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        if ($typename == "Form" && $group == "MA") {
            $destinationPath = 'uploads/form/ma';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        if ($typename == "Form" && $group == "PMC") {
            $destinationPath = 'uploads/form/pmc';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        if ($typename == "Form" && $group == "PP") {
            $destinationPath = 'uploads/form/pp';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        if ($typename == "Form" && $group == "REQM") {
            $destinationPath = 'uploads/form/reqm';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        if ($typename == "Form" && $group == "SQA") {
            $destinationPath = 'uploads/form/sqa';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        if ($typename == "Form" && $group == "SCM") {
            $destinationPath = 'uploads/form/scm';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        if ($typename == "Form" && $group == "OTHER") {
            $destinationPath = 'uploads/form/other';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        if ($typename == "RF" && $group == "MASTERLIST") {
            $destinationPath = 'uploads/ref/masterlist';
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        DB::insert('INSERT INTO cmmi_doc (doc_type_id,doc_shortname,doc_longname,doc_path,doc_head) values(?,?,?,?,?)', [$group, $pname, $fname, $fillename, $typename]);

        return redirect('/insert');
    }


    // public function selectwi()
    // {
    //     $docCM = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="WI" AND doc_type_id="CM" ');
    //     $docMA = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="WI" AND doc_type_id="MA" ');
    //     $docORG = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="WI" AND doc_type_id="ORG" ');
    //     $docPMC = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="WI" AND doc_type_id="PMC" ');
    //     $docPPQA = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="WI" AND doc_type_id="PPQA" ');
    //     $docPP = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="WI" AND doc_type_id="PP" ');
    //     $docREQM = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="WI" AND doc_type_id="REQM" ');
    //     $docSAM = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="WI" AND doc_type_id="SAM" ');

    //     return view('hpage/wi', ['docCM' => $docCM, 'docMA' => $docMA, 'docORG' => $docORG, 'docPMC' => $docPMC, 'docPPQA' => $docPPQA, 'docPP' => $docPP, 'docREQM' => $docREQM, 'docSAM' => $docSAM]);
    // }

    public function selectum()
    {
        $umCM = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="UM" AND doc_type_id="CM"');
        $umMA = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="UM" AND doc_type_id="MA"');
        $umORG = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="UM" AND doc_type_id="ORG"');
        $umPMC = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="UM" AND doc_type_id="PMC"');
        $umPP = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="UM" AND doc_type_id="PP"');
        $umREQM = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="UM" AND doc_type_id="REQM"');
        $umSQA = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="UM" AND doc_type_id="SQA"');
        $umSCM = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="UM" AND doc_type_id="SCM"');

        return view('hpage/um', ['umCM' => $umCM, 'umMA' => $umMA, 'umORG' => $umORG, 'umPMC' => $umPMC, 'umPP' => $umPP, 'umREQM' => $umREQM, 'umSQA' => $umSQA, 'umSCM' => $umSCM]);
    }

    public function selectform()
    {
        $fmMA = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="Form" AND doc_type_id="MA"');
        $fmPMC = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="Form" AND doc_type_id="PMC"');
        $fmPP = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="Form" AND doc_type_id="PP"');
        $fmREQM = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="Form" AND doc_type_id="REQM"');
        $fmSQA = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="Form" AND doc_type_id="SQA"');
        $fmSCM = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="Form" AND doc_type_id="SCM"');
        $fmOTHER = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="Form" AND doc_type_id="OTHER"');

        return view('hpage/form', ['fmMA' => $fmMA, 'fmPMC' => $fmPMC, 'fmPP' => $fmPP, 'fmREQM' => $fmREQM, 'fmSQA' => $fmSQA, 'fmSCM' => $fmSCM, 'fmOTHER' => $fmOTHER]);
    }

    public function selectmanage(Request $request)
    {
       
            $typename = $request->input('tp');
            $group = $request->input('group');


            if ($typename != "" || $group != "") {
                $manageall = DB::select('SELECT * FROM cmmi_doc WHERE doc_head = "' . $typename . '" OR doc_type_id = "' . $group . '"');
                return view('hpage/manage', ['manageall' => $manageall]);
            } else {
                $manageall = DB::select('SELECT * FROM cmmi_doc');
                return view('hpage/manage', ['manageall' => $manageall]);
            }
        
    }

    public function destroy(Request $request)
    {
        $id = $request->input('dcid');

        DB::delete('DELETE FROM cmmi_doc where doc_id = ? ', [$id]);

        return redirect('/manage');
    }

    public function edidview(Request $request)
    {
        $id = $request->input('dcid');
        $eddoc = DB::select('SELECT * FROM cmmi_doc WHERE doc_id= ? ', [$id]);

        return view('hpage/editdoc', ['eddoc' => $eddoc]);
    }

    public function edicoed(Request $request)
    {
        $id = $request->input('docid');
        $fname = $request->input('fname');
        $pname = $request->input('pname');
        $typename = $request->input('tp');
        $group = $request->input('group');
        $filehid = $request->input('filehid');
        $file = $request->file('file');


        if ($file != "") {
            $fillename = $file->getClientOriginalName();

            if ($typename == "UM" && $group == "CM") {
                $destinationPath = 'uploads/um/cm';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            if ($typename == "UM" && $group == "MA") {
                $destinationPath = 'uploads/um/ma';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            if ($typename == "UM" && $group == "ORG") {
                $destinationPath = 'uploads/um/org';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            if ($typename == "UM" && $group == "PMC") {
                $destinationPath = 'uploads/um/pmc';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            if ($typename == "UM" && $group == "PP") {
                $destinationPath = 'uploads/um/pp';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            if ($typename == "UM" && $group == "REQM") {
                $destinationPath = 'uploads/um/reqm';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            if ($typename == "UM" && $group == "SQA") {
                $destinationPath = 'uploads/um/sqa';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            if ($typename == "UM" && $group == "SCM") {
                $destinationPath = 'uploads/um/scm';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            if ($typename == "Form" && $group == "MA") {
                $destinationPath = 'uploads/form/ma';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            if ($typename == "Form" && $group == "PMC") {
                $destinationPath = 'uploads/form/pmc';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            if ($typename == "Form" && $group == "PP") {
                $destinationPath = 'uploads/form/pp';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            if ($typename == "Form" && $group == "REQM") {
                $destinationPath = 'uploads/form/reqm';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            if ($typename == "Form" && $group == "SQA") {
                $destinationPath = 'uploads/form/sqa';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            if ($typename == "Form" && $group == "SCM") {
                $destinationPath = 'uploads/form/scm';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            if ($typename == "Form" && $group == "OTHER") {
                $destinationPath = 'uploads/form/other';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            if ($typename == "RF" && $group == "MASTERLIST") {
                $destinationPath = 'uploads/ref/masterlist';
                $file->move($destinationPath, $file->getClientOriginalName());
            }

            DB::update('UPDATE cmmi_doc SET doc_type_id = ? , doc_shortname = ? , doc_longname = ? , doc_path = ? , doc_head = ? WHERE doc_id = ? ', [$group, $pname, $fname, $fillename, $typename, $id]);

            return redirect('/manage');
        } else {
            DB::update('UPDATE cmmi_doc SET doc_type_id = ? , doc_shortname = ? , doc_longname = ? , doc_path = ? , doc_head = ? WHERE doc_id = ? ', [$group, $pname, $fname, $filehid, $typename, $id]);

            return redirect('/manage');
        }
    }


    // public function plogin(LoginRequest $request)
    // {
    //     $username = $request->input('username');
    //     $password = $request->input('password');
        
    //     if(Auth::attempt(['username' => $username, 'password' => $password])){
    //         return redirect()->intended('/manage');
    //     }else{
    //        echo '2222';
    //     }

       
    // }


    public function selectref()
    {
        $umMS = DB::select('SELECT * FROM cmmi_doc WHERE doc_head="RF" AND doc_type_id="MASTERLIST"');


        return view('hpage/ref', ['umMS' => $umMS]);
    }

    public function login(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        if ($username == 'administrator' && $password == 'km@2019sd1') {
            return redirect('/manage');
        } else {
            echo "<script>";
            echo "alert(\" user หรือ  password ไม่ถูกต้อง\");";
            echo "window.history.back()";
            echo "</script>";
        }
    }
}
