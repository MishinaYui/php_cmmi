<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>PCCTH CMMI</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{asset('vendor/assets/img/favicon.jpg')}}" rel="icon">
    <link href="{{asset('vendor/assets/img/apple-touch-icon.jpg')}}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('vendor/assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/icofont/icofont.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendot/assets/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/venobox/venobox.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/aos/aos.css')}}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{asset('vendor/assets/css/style.css')}}" rel="stylesheet">

    <!-- =======================================================
  * Template Name: Vesperr - v2.0.0
  * Template URL: https://bootstrapmade.com/vesperr-free-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center">

            <div class="logo mr-auto">
                <h1 class="text-light"><a href="#"><span>PCCTH CMMI</span></a></h1>

            </div>

            <nav class="nav-menu d-none d-lg-block">
                <ul>
                    <li class="active"><a href="/">Logout</a></li>

                </ul>
            </nav><!-- .nav-menu -->

        </div>
    </header><!-- End Header -->

    <main id="main">

        <!-- ======= About Us Section ======= -->
        <section id="about" class="about">
            <div class="container">
                @csrf

                <div class="section-title" data-aos="fade-up">
                    <br>
                    <br>
                    <h2>ManageMent</h2>
                </div>
                <br>
                <br>
                <form action="/manage" method="GET">
                    <div class="container-fluid table-responsive form-row">
                        <br>
                        <div class="form-group col-md-3">
                            <select id="tp" name="tp" class="form-control">
                                <option value="" selected>-- กรุณาเลือก ประเภทเอกสาร --</option>
                                <option value="Form">Form</option>
                                <option value="UM">คู่มือการปฏิบัติงาน</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <select id="group" name="group" class="form-control">
                                <option value="" selected>-- กรุณาเลือก หมวดหมู่เอกสาร --</option>
                                <option value="CM">CM **(เฉพาะประเภทเอกสาร คู่มือ เท่านั้น) </option>
                                <option value="MA">MA</option>
                                <option value="ORG">ORG</option>
                                <option value="PMC">PMC</option>
                                <option value="PPQA">PPQA</option>
                                <option value="PP">PP</option>
                                <option value="REQM">REQM</option>
                                <option value="SQA">SQA **(เฉพาะประเภทเอกสาร คู่มือ และ Form เท่านั้น) </option>
                                <option value="SCM">SCM **(เฉพาะประเภทเอกสาร คู่มือ และ Form เท่านั้น) </option>
                                <option value="OTHER">OTHER **(เฉพาะประเภทเอกสาร Form) </option>
                            </select>
                        </div>
                        <div class="form-group col-md-1">
                            <left>
                                <button type="submit" class="btn btn-primary mb-2">ค้นหา</button>
                            </left>
                        </div>

                        <div class="form-group col-md-2">
                            <left>
                                <a href="/insert"><button type="button" class="btn btn-warning mb-2">เพิ่มข้อมูล</button></a>
                            </left>
                        </div>

                    </div>
                </form>

                <br>

                <div class="row content table-responsive">
                    <div class="col-lg-12" data-aos="fade-up" data-aos-delay="450">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">ชื่อเอกสาร</th>
                                    <th scope="col">รหัสเอกสาร</th>
                                    <th scope="col">ประเภทเอกสาร</th>
                                    <th scope="col">หมวดหมู่เอกสาร</th>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($manageall as $ma)
                                <tr>
                                    <td>{{ $ma->doc_id }}</td>
                                    <td>{{ $ma->doc_longname }}</td>
                                    <td>{{ $ma->doc_shortname }}</td>
                                    <td>{{ $ma->doc_head }}</td>
                                    <td>{{ $ma->doc_type_id }}</td>
                                    <td>
                                        <form action="/edit" method="GET">
                                            <input type="hidden" name="dcid" value="{{ $ma->doc_id }}">
                                            <button class="btn btn-success" type="submit">แก้ไข</button>
                                        </form>
                                    </td>
                                    <td>
                                        <form action="/delete" method="GET">
                                            <input type="hidden" name="dcid" value="{{ $ma->doc_id }}">
                                            <button class="btn btn-danger" type="submit">ลบ</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>

            </div>
        </section><!-- End About Us Section -->

    </main><!-- End #main -->




    <!-- ======= Footer ======= -->
    <footer id="footer">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-lg-6 text-lg-left text-center">
                    <div class="copyright">
                        &copy; Copyright <strong>PCCTH.COM</strong>. All Rights Reserved
                    </div>
                    <div class="credits">
                        <!-- All the links in the footer should remain intact. -->
                        <!-- You can delete the links only if you purchased the pro version. -->
                        <!-- Licensing information: https://bootstrapmade.com/license/ -->
                        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/vesperr-free-bootstrap-template/ -->
                        Designed by SD1
                    </div>
                </div>
            </div>
        </div>
    </footer><!-- End Footer -->

    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{asset('vendor/assets/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/assets/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('vendor/assets/jquery.easing/jquery.easing.min.js')}}"></script>
    <script src="{{asset('vendor/assets/php-email-form/validate.js')}}"></script>
    <script src="{{asset('vendor/assets/waypoints/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('vendor/assets/counterup/counterup.min.js')}}"></script>
    <script src="{{asset('vendor/assets/owl.carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('vendor/assets/isotope-layout/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('vendor/assets/venobox/venobox.min.js')}}"></script>
    <script src="{{asset('vendor/assets/aos/aos.js')}}"></script>

    <!-- Template Main JS File -->
    <script src="{{asset('vendor/assets/js/main.js')}}"></script>

    
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>


</body>

</html>