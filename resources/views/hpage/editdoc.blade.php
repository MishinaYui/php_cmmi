<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>PCCTH CMMI</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{asset('vendor/assets/img/favicon.jpg')}}" rel="icon">
    <link href="{{asset('vendor/assets/img/apple-touch-icon.jpg')}}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('vendor/assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/icofont/icofont.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendot/assets/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/venobox/venobox.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/aos/aos.css')}}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{asset('vendor/assets/css/style.css')}}" rel="stylesheet">

    <!-- =======================================================
  * Template Name: Vesperr - v2.0.0
  * Template URL: https://bootstrapmade.com/vesperr-free-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->



</head>

<body>
    <script>
        function fncSubmit() {

            var file = document.doc1.file.value;
            var filetype = file.substr(-3);


            if (document.doc1.fname.value == "") {
                alert("กรุณากรอกชื่อเอกสาร");
                document.doc1.fname.focus();
                return false;
            }

            if (document.doc1.pname.value == "") {
                alert("กรุณากรอกรหัสเอกสาร");
                document.doc1.pname.focus();
                return false;
            }

            if (document.doc1.tp.value == "") {
                alert("กรุณาเลือกประเภทเอกสาร");
                document.doc1.tp.focus();
                return false;
            }

            if (document.doc1.group.value == "") {
                alert("กรุณาเลือกหมวดหมู่เอกสาร")
                document.doc1.group.focus();
                return false;
            }

            // if (document.doc1.file.value == "") {
            //     alert("กรุณาเลือกไฟล์")
            //     document.doc1.file.focus();
            //     return false;
            // }



            // if (document.doc1.tp.value == "WI" && document.doc1.group.value == "SQA") {
            //     alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
            //     document.doc1.group.focus();
            //     return false;
            // }

            // if (document.doc1.tp.value == "WI" && document.doc1.group.value == "SMC") {
            //     alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
            //     document.doc1.group.focus();
            //     return false;
            // }

            // if (document.doc1.tp.value == "WI" && document.doc1.group.value == "OTHER") {
            //     alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
            //     document.doc1.group.focus();
            //     return false;
            // }

            if (document.doc1.tp.value == "UM" && document.doc1.group.value == "PPQA") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "UM" && document.doc1.group.value == "SAM") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "UM" && document.doc1.group.value == "OTHER") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "๊UM" && document.doc1.group.value == "MASTERLIST") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "Form" && document.doc1.group.value == "CM") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "Form" && document.doc1.group.value == "PPQA") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "Form" && document.doc1.group.value == "SAM") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "Form" && document.doc1.group.value == "MASTERLIST") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "RF" && document.doc1.group.value == "CM") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "RF" && document.doc1.group.value == "MA") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "RF" && document.doc1.group.value == "ORG") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "RF" && document.doc1.group.value == "PMC") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "RF" && document.doc1.group.value == "PPQA") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "RF" && document.doc1.group.value == "REQM") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "RF" && document.doc1.group.value == "SQA") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "RF" && document.doc1.group.value == "SCM") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }

            if (document.doc1.tp.value == "RF" && document.doc1.group.value == "OTHER") {
                alert('กรุณาเลือกหมวดหมู่เอกสารให้ถูกต้อง');
                document.doc1.group.focus();
                return false;
            }


            // if (document.doc1.tp.value == "WI" && filetype != "pdf") {
            //     alert("กรุราเลือกไฟล์เป็น PDF");
            //     document.doc1.file.focus();
            //     return false;
            // }

            if (document.doc1.tp.value == "UM" && filetype != "pdf") {
                alert("กรุราเลือกไฟล์เป็น PDF");
                document.doc1.file.focus();
                return false;
            }



        }
    </script>
    <form name="doc1" action="/editcode" method="POST" enctype="multipart/form-data" onSubmit="JavaScript:return fncSubmit();">
        <!-- ======= Header ======= -->
        <header id="header" class="fixed-top d-flex align-items-center">
            <div class="container d-flex align-items-center">

                <div class="logo mr-auto">
                    <h1 class="text-light"><a href="#"><span>PCCTH CMMI</span></a></h1>

                </div>

                <nav class="nav-menu d-none d-lg-block">
                    <ul>
                        <li class="active"><a href="/">Logout</a></li>

                    </ul>
                </nav><!-- .nav-menu -->

            </div>
        </header><!-- End Header -->



        <main id="main">

            <!-- ======= About Us Section ======= -->
            <section id="about" class="about">



                @csrf

                <div class="container">

                    <div class="section-title" data-aos="fade-up">
                        <h2>Insert</h2>
                    </div>

                        <div class="row content">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-10" data-aos="fade-up" data-aos-delay="300">
                                @foreach ($eddoc as $edddoc)
                                <br>
                                <label for="fname">ชื่อเอกสาร : </label>
                                <input type="hidden" class="form-control" name="docid" id="docid" value="{{ $edddoc->doc_id }}">
                                <input type="text" class="form-control" name="fname" id="fname" value="{{ $edddoc->doc_longname }}">


                                <br>
                                <label for="pname">รหัสเอกสาร : </label>
                                <input type="text" class="form-control" name="pname" id="pname" value="{{ $edddoc->doc_shortname }}">

                                <br>
                                <label for="tp">ประเภทเอกสาร : </label>
                                <select id="tp" name="tp" class="form-control">
                                    <option value="{{ $edddoc->doc_head }}" selected>{{ $edddoc->doc_head }}</option>
                                    <option value="Form">Form</option>
                                    <option value="UM">คู่มือการปฏิบัติงาน</option>
                                </select>

                                <br>
                                <label for="group">หมวดหมู่เอกสาร :</label>
                                <select id="group" name="group" class="form-control">
                                    <option value="{{ $edddoc->doc_type_id }}" selected>{{ $edddoc->doc_type_id }}</option>
                                    <option value="CM">CM **(เฉพาะประเภทเอกสาร คู่มือ เท่านั้น) </option>
                                    <option value="MA">MA</option>
                                    <option value="ORG">ORG</option>
                                    <option value="PMC">PMC</option>
                                    <option value="PPQA">PPQA</option>
                                    <option value="PP">PP</option>
                                    <option value="REQM">REQM</option>
                                    <option value="SQA">SQA **(เฉพาะประเภทเอกสาร คู่มือ และ Form เท่านั้น) </option>
                                    <option value="SCM">SCM **(เฉพาะประเภทเอกสาร คู่มือ และ Form เท่านั้น) </option>
                                    <option value="OTHER">OTHER **(เฉพาะประเภทเอกสาร Form) </option>

                                </select>

                                <br>
                                <label for="name">File : &nbsp;<font style="color: red">**(กรุณาเลือกไฟล์ .DOC .XLS .PDF ประเภทเอกสาร WI และ คู่มื่อ อัพโหลดได้เฉพาะไฟล์ .PDF เท่านั้น)</font></label>
                                <input type="file" class="form-control" name="file" id="file" multiple>
                                <input type="hidden" class="form-control" name="filehid" id="filehid" value="{{ $edddoc->doc_path }}" id="file">

                                <br><br>
                                <center><button type="submit" class="btn btn-primary">ตกลง</button></center>

                                @endforeach

                            </div>
                            <div class="col-lg-1"></div>
                        </div>
                   
                </div>

            </section><!-- End About Us Section -->

        </main><!-- End #main -->

        <!-- ======= Footer ======= -->
        <footer id="footer">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-6 text-lg-left text-center">
                        <div class="copyright">
                            &copy; Copyright <strong>PCCTH.COM</strong>. All Rights Reserved
                        </div>
                        <div class="credits">
                            <!-- All the links in the footer should remain intact. -->
                            <!-- You can delete the links only if you purchased the pro version. -->
                            <!-- Licensing information: https://bootstrapmade.com/license/ -->
                            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/vesperr-free-bootstrap-template/ -->
                            Designed by SD1
                        </div>
                    </div>
                </div>
            </div>
        </footer><!-- End Footer -->
    </form>

    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{asset('vendor/assets/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/assets/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('vendor/assets/jquery.easing/jquery.easing.min.js')}}"></script>
    <script src="{{asset('vendor/assets/php-email-form/validate.js')}}"></script>
    <script src="{{asset('vendor/assets/waypoints/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('vendor/assets/counterup/counterup.min.js')}}"></script>
    <script src="{{asset('vendor/assets/owl.carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('vendor/assets/isotope-layout/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('vendor/assets/venobox/venobox.min.js')}}"></script>
    <script src="{{asset('vendor/assets/aos/aos.js')}}"></script>

    <!-- Template Main JS File -->
    <script src="{{asset('vendor/assets/js/main.js')}}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

</body>

</html>