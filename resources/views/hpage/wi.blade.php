<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>PCCTH CMMI</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{asset('vendor/assets/img/favicon.jpg')}}" rel="icon">
    <link href="{{asset('vendor/assets/img/apple-touch-icon.jpg')}}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('vendor/assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/icofont/icofont.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendot/assets/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/venobox/venobox.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/aos/aos.css')}}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{asset('vendor/assets/css/style.css')}}" rel="stylesheet">

    <!-- =======================================================
  * Template Name: Vesperr - v2.0.0
  * Template URL: https://bootstrapmade.com/vesperr-free-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center">

            <div class="logo mr-auto">
                <h1 class="text-light"><a href="#"><span>PCCTH CMMI</span></a></h1>

            </div>

            <nav class="nav-menu d-none d-lg-block">
                <ul>
                    <li class="active"><a href="/">Home</a></li>
                    <li><a href="/um">คู่มือการปฏิบัติงาน</a></li>
                    <li><a href="/form">Form</a></li>
                    <li><a href="/ref">Reference</a></li>
                    <li><a href="#">News</a></li>
                    <li><a href="/login">Login</a></li>
                </ul>
            </nav><!-- .nav-menu -->

        </div>
    </header><!-- End Header -->


    <main id="main">

        <!-- ======= About Us Section ======= -->
        <section id="about" class="about">
            <div class="container">
                @csrf

                <div class="section-title" data-aos="fade-up">
                    <h2>WI</h2>
                </div>


                <div class="row content">
                    <div class="col-lg-12" data-aos="fade-up" data-aos-delay="450">

                        <ul class="list-group">
                            <li class="list-group-item" id="showCM"">CM</li>
                            @foreach ($docCM as $doccm)
                            <a href=" {{asset('uploads/wi/cm').'/'.$doccm->doc_path }}" target="_blak">
                            <li class=" list-group-item showsCM" id="CM" style="display: none;"> <span class="bx bx-book-open"> &nbsp;</span>{{ $doccm->doc_shortname }} : {{ $doccm->doc_longname }}</li></a>
                            @endforeach
                        </ul>

                        <ul class="list-group">
                            <li class="list-group-item" id="showMA"">MA</li>
                            @foreach ($docMA as $docma)
                            <a href=" {{asset('uploads/wi/ma').'/'.$docma->doc_path }}" target="_blak">
                            <li class=" list-group-item showsMA" id="MA" style="display: none;"> <span class="bx bx-book-open"> &nbsp;</span>{{ $docma->doc_shortname }} : {{ $docma->doc_longname }}</li></a>
                            @endforeach
                        </ul>


                        <ul class="list-group">
                            <li class="list-group-item" id="showORG"">ORG</li>
                            @foreach ($docORG as $doccorg)
                            <a href=" {{asset('uploads/wi/org').'/'.$doccorg->doc_path }}" target="_blak">
                            <li class=" list-group-item showsORG" id="ORG" style="display: none;"> <span class="bx bx-book-open"> &nbsp;</span>{{ $doccorg->doc_shortname }} : {{ $doccorg->doc_longname }}</li></a>
                            @endforeach
                        </ul>

                        <ul class="list-group">
                            <li class="list-group-item" id="showpmc"">PMC</li>
                            @foreach ($docPMC as $doccpmc)
                            <a href=" {{asset('uploads/wi/pmc').'/'.$doccpmc->doc_path }}" target="_blak">
                            <li class=" list-group-item showspmc" id="ORG" style="display: none;"> <span class="bx bx-book-open"> &nbsp;</span>{{ $doccpmc->doc_shortname }} : {{ $doccpmc->doc_longname }}</li></a>
                            @endforeach
                        </ul>

                        <ul class="list-group">
                            <li class="list-group-item" id="showppqa"">PPQA</li>
                            @foreach ($docPPQA as $doccppqa)
                            <a href=" {{asset('uploads/wi/ppqa').'/'.$doccppqa->doc_path }}" target="_blak">
                            <li class=" list-group-item showsppqa" id="ORG" style="display: none;"> <span class="bx bx-book-open"> &nbsp;</span>{{ $doccppqa->doc_shortname }} : {{ $doccppqa->doc_longname }}</li></a>
                            @endforeach
                        </ul>

                        <ul class="list-group">
                            <li class="list-group-item" id="showpp"">PP</li>
                            @foreach ($docPP as $doccpp)
                            <a href=" {{asset('uploads/wi/pp').'/'.$doccpp->doc_path }}" target="_blak">
                            <li class=" list-group-item showspp" id="ORG" style="display: none;"> <span class="bx bx-book-open"> &nbsp;</span>{{ $doccpp->doc_shortname }} : {{ $doccpp->doc_longname }}</li></a>
                            @endforeach
                        </ul>

                        <ul class="list-group">
                            <li class="list-group-item" id="showreqm"">REQM</li>
                            @foreach ($docREQM as $doccreqm)
                            <a href=" {{asset('uploads/wi/reqm').'/'.$doccreqm->doc_path }}" target="_blak">
                            <li class=" list-group-item showsreqm" id="ORG" style="display: none;"> <span class="bx bx-book-open"> &nbsp;</span>{{ $doccreqm->doc_shortname }} : {{ $doccreqm->doc_longname }}</li></a>
                            @endforeach
                        </ul>

                        <ul class="list-group">
                            <li class="list-group-item" id="showsam"">SAM</li>
                            @foreach ($docSAM as $doccsam)
                            <a href=" {{asset('uploads/wi/sam').'/'.$doccsam->doc_path }}" target="_blak">
                            <li class=" list-group-item showssam" id="ORG" style="display: none;"> <span class="bx bx-book-open"> &nbsp;</span>{{ $doccsam->doc_shortname }} : {{ $doccsam->doc_longname }}</li></a>
                            @endforeach
                        </ul>


                    </div>

                </div>

            </div>
        </section><!-- End About Us Section -->

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-lg-6 text-lg-left text-center">
                    <div class="copyright">
                        &copy; Copyright <strong>PCCTH.COM</strong>. All Rights Reserved
                    </div>
                    <div class="credits">
                        <!-- All the links in the footer should remain intact. -->
                        <!-- You can delete the links only if you purchased the pro version. -->
                        <!-- Licensing information: https://bootstrapmade.com/license/ -->
                        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/vesperr-free-bootstrap-template/ -->
                        Designed by SD1
                    </div>
                </div>
            </div>
        </div>
    </footer><!-- End Footer -->

    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{asset('vendor/assets/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/assets/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('vendor/assets/jquery.easing/jquery.easing.min.js')}}"></script>
    <script src="{{asset('vendor/assets/php-email-form/validate.js')}}"></script>
    <script src="{{asset('vendor/assets/waypoints/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('vendor/assets/counterup/counterup.min.js')}}"></script>
    <script src="{{asset('vendor/assets/owl.carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('vendor/assets/isotope-layout/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('vendor/assets/venobox/venobox.min.js')}}"></script>
    <script src="{{asset('vendor/assets/aos/aos.js')}}"></script>

    <!-- Template Main JS File -->
    <script src="{{asset('vendor/assets/js/main.js')}}"></script>


    <script>
        //----- show/hide CM --------------------
        $(document).ready(function() {
            $("#showCM").click(function() {
                $(".showsCM").toggle({
                    duration: 1000,
                });
            });
        });

        //------- show/hide MA -------------------

        $(document).ready(function() {
            $("#showMA").click(function() {
                $(".showsMA").toggle({
                    duration: 1000,
                });
            });
        });

        //-------- show/hie ORG ---------------

        $(document).ready(function() {
            $("#showORG").click(function() {
                $(".showsORG").toggle({
                    duration: 1000,
                });
            });
        });

        //---------- show/hid PMC ------------

        $(document).ready(function() {
            $("#showpmc").click(function() {
                $(".showspmc").toggle({
                    duration: 1000,
                });
            });
        });

        //--------- show/hide PPQA ------------

        $(document).ready(function() {
            $("#showppqa").click(function() {
                $(".showsppqa").toggle({
                    duration: 1000,
                });
            });
        });

        //--------- show/hide PP ---------------

        $(document).ready(function() {
            $("#showpp").click(function() {
                $(".showspp").toggle({
                    duration: 1000,
                });
            });
        });

        //---------- show/hide REQM -------------

        $(document).ready(function() {
            $("#showreqm").click(function() {
                $(".showsreqm").toggle({
                    duration: 1000,
                });
            });
        });

        //------------ show/hide SAM -----------

        $(document).ready(function() {
            $("#showsam").click(function() {
                $(".showssam").toggle({
                    duration: 1000,
                });
            });
        });
    </script>

</body>

</html>