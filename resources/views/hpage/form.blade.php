<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>PCCTH CMMI</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{asset('vendor/assets/img/favicon.jpg')}}" rel="icon">
    <link href="{{asset('vendor/assets/img/apple-touch-icon.jpg')}}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('vendor/assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/icofont/icofont.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendot/assets/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/venobox/venobox.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/aos/aos.css')}}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{asset('vendor/assets/css/style.css')}}" rel="stylesheet">

    <!-- =======================================================
  * Template Name: Vesperr - v2.0.0
  * Template URL: https://bootstrapmade.com/vesperr-free-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>


<body>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center">

            <div class="logo mr-auto">
                <h1 class="text-light"><a href="#"><span>PCCTH CMMI</span></a></h1>

            </div>

            <nav class="nav-menu d-none d-lg-block">
            <ul>
            <li class="active"><a href="/">Home</a></li>
                    <li><a href="/um">คู่มือการปฏิบัติงาน</a></li>
                    <li><a href="/form">Form</a></li>
                    <li><a href="/ref">Reference</a></li>
                    <li><a href="#">News</a></li>
                    <li><a href="/login">Login</a></li>
                </ul>
            </nav><!-- .nav-menu -->

        </div>
    </header><!-- End Header -->

    <main id="main">

        <!-- ======= About Us Section ======= -->
        <section id="about" class="about">
            <div class="container">
                @csrf

                <div class="section-title" data-aos="fade-up">
                    <h2>Form</h2>
                </div>
                <div class="row content">
                    <div class="col-lg-12" data-aos="fade-up" data-aos-delay="450">

                        <ul class="list-group">
                            <li class="list-group-item" id="showMA">MA</li>
                            @foreach ($fmMA as $fmmMA)
                            <a href=" {{asset('uploads/form/ma').'/'.$fmmMA->doc_path }}" target="_blak">
                                <li class=" list-group-item showsMA" id="MA" style="display: none;"> <span class="bx bx-book-open"> &nbsp;</span>{{ $fmmMA->doc_shortname }} : {{ $fmmMA->doc_longname }}</li>
                            </a>
                            @endforeach
                        </ul>

                        <ul class="list-group">
                            <li class="list-group-item" id="showpmc">PMC</li>
                            @foreach ($fmPMC as $fmmpmc)
                            <a href=" {{asset('uploads/form/pmc').'/'.$fmmpmc->doc_path }}" target="_blak">
                                <li class=" list-group-item showspmc" id="ORG" style="display: none;"> <span class="bx bx-book-open"> &nbsp;</span>{{ $fmmpmc->doc_shortname }} : {{ $fmmpmc->doc_longname }}</li>
                            </a>
                            @endforeach
                        </ul>

                        <ul class="list-group">
                            <li class="list-group-item" id="showpp">PP</li>
                            @foreach ($fmPP as $fmmpp)
                            <a href=" {{asset('uploads/form/pp').'/'.$fmmpp->doc_path }}" target="_blak">
                                <li class=" list-group-item showspp" id="ORG" style="display: none;"> <span class="bx bx-book-open"> &nbsp;</span>{{ $fmmpp->doc_shortname }} : {{ $fmmpp->doc_longname }}</li>
                            </a>
                            @endforeach
                        </ul>

                        <ul class="list-group">
                            <li class="list-group-item" id="showreqm">REQM</li>
                            @foreach ($fmREQM as $fmmreqm)
                            <a href=" {{asset('uploads/form/reqm').'/'.$fmmreqm->doc_path }}" target="_blak">
                                <li class=" list-group-item showsreqm" id="ORG" style="display: none;"> <span class="bx bx-book-open"> &nbsp;</span>{{ $fmmreqm->doc_shortname }} : {{ $fmmreqm->doc_longname }}</li>
                            </a>
                            @endforeach
                        </ul>

                        <ul class="list-group">
                            <li class="list-group-item" id="showsqa">SQA</li>
                            @foreach ($fmSQA as $fmmsqa)
                            <a href=" {{asset('uploads/form/sqa').'/'.$fmmsqa->doc_path }}" target="_blak">
                                <li class=" list-group-item showssqa" id="SQA" style="display: none;"> <span class="bx bx-book-open"> &nbsp;</span>{{ $fmmsqa->doc_shortname }} : {{ $fmmsqa->doc_longname }}</li>
                            </a>
                            @endforeach
                        </ul>

                        <ul class="list-group">
                            <li class="list-group-item" id="showscm">SCM</li>
                            @foreach ($fmSCM as $fmmscm)
                            <a href=" {{asset('uploads/form/scm').'/'.$fmmscm->doc_path }}" target="_blak">
                                <li class=" list-group-item showsscm" id="SCM" style="display: none;"> <span class="bx bx-book-open"> &nbsp;</span>{{ $fmmscm->doc_shortname }} : {{ $fmmscm->doc_longname }}</li>
                            </a>
                            @endforeach
                        </ul>

                        <ul class="list-group">
                            <li class="list-group-item" id="showother">OTHER</li>
                            @foreach ($fmOTHER as $fmmother)
                            <a href=" {{asset('uploads/form/other').'/'.$fmmother->doc_path }}" target="_blak">
                                <li class=" list-group-item showsother" id="OTHER" style="display: none;"> <span class="bx bx-book-open"> &nbsp;</span>{{ $fmmother->doc_shortname }} : {{ $fmmother->doc_longname }}</li>
                            </a>
                            @endforeach
                        </ul>




                    </div>

                </div>

            </div>
        </section><!-- End About Us Section -->

    </main><!-- End #main -->




    <!-- ======= Footer ======= -->
    <footer id="footer">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-lg-6 text-lg-left text-center">
                    <div class="copyright">
                        &copy; Copyright <strong>PCCTH.COM</strong>. All Rights Reserved
                    </div>
                    <div class="credits">
                        <!-- All the links in the footer should remain intact. -->
                        <!-- You can delete the links only if you purchased the pro version. -->
                        <!-- Licensing information: https://bootstrapmade.com/license/ -->
                        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/vesperr-free-bootstrap-template/ -->
                        Designed by SD1
                    </div>
                </div>
            </div>
        </div>
    </footer><!-- End Footer -->

    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{asset('vendor/assets/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/assets/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('vendor/assets/jquery.easing/jquery.easing.min.js')}}"></script>
    <script src="{{asset('vendor/assets/php-email-form/validate.js')}}"></script>
    <script src="{{asset('vendor/assets/waypoints/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('vendor/assets/counterup/counterup.min.js')}}"></script>
    <script src="{{asset('vendor/assets/owl.carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('vendor/assets/isotope-layout/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('vendor/assets/venobox/venobox.min.js')}}"></script>
    <script src="{{asset('vendor/assets/aos/aos.js')}}"></script>

    <!-- Template Main JS File -->
    <script src="{{asset('vendor/assets/js/main.js')}}"></script>

    <script>
      


        //------- show/hide MA -------------------

        $(document).ready(function() {
            $("#showMA").click(function() {
                $(".showsMA").toggle({
                    duration: 1000,
                });
            });
        });


        //---------- show/hid PMC ------------

        $(document).ready(function() {
            $("#showpmc").click(function() {
                $(".showspmc").toggle({
                    duration: 1000,
                });
            });
        });

        //--------- show/hide PP ---------------

        $(document).ready(function() {
            $("#showpp").click(function() {
                $(".showspp").toggle({
                    duration: 1000,
                });
            });
        });

        //---------- show/hide REQM -------------

        $(document).ready(function() {
            $("#showreqm").click(function() {
                $(".showsreqm").toggle({
                    duration: 1000,
                });
            });
        });

        //---------- show/hide SQA -------------

        $(document).ready(function() {
            $("#showsqa").click(function() {
                $(".showssqa").toggle({
                    duration: 1000,
                });
            });
        });

        //--------- show/hide SCM ---------------

        $(document).ready(function() {
            $("#showscm").click(function() {
                $(".showsscm").toggle({
                    duration: 1000,
                });
            });
        });

        //------- show/hide OTHER ---------------

        $(document).ready(function() {
            $("#showother").click(function() {
                $(".showsother").toggle({
                    duration: 1000,
                });
            });
        });
    </script>




</body>

</html>