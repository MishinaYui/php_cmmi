<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>PCCTH CMMI</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{asset('vendor/assets/img/favicon.jpg')}}" rel="icon">
    <link href="{{asset('vendor/assets/img/apple-touch-icon.jpg')}}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('vendor/assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/icofont/icofont.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendot/assets/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/venobox/venobox.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/assets/aos/aos.css')}}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{asset('vendor/assets/css/style.css')}}" rel="stylesheet">

    <!-- =======================================================
  * Template Name: Vesperr - v2.0.0
  * Template URL: https://bootstrapmade.com/vesperr-free-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->



</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center">

            <div class="logo mr-auto">
                <h1 class="text-light"><a href="#"><span>PCCTH CMMI</span></a></h1>

            </div>

            <nav class="nav-menu d-none d-lg-block">
                <ul>
                <li class="active"><a href="/">Home</a></li>
                    <li><a href="/um">คู่มือการปฏิบัติงาน</a></li>
                    <li><a href="/form">Form</a></li>
                    <li><a href="/ref">Reference</a></li>
                    <li><a href="#">News</a></li>
                    <li><a href="/login">Login</a></li>
                </ul>
            </nav><!-- .nav-menu -->

        </div>
    </header><!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex align-items-center">

        <div class="container">

            <div class="col-lg-12 order-1 order-lg-2 hero-img" data-aos="fade-left" data-aos-delay="600">
                <img src="{{asset('vendor/assets/img/homeban.jpg')}}" class="img-fluid animated" alt="">
            </div>
        </div>
        </div>

    </section><!-- End Hero -->

    <main id="main">

        <!-- ======= About Us Section ======= -->
        <section id="about" class="about">
            <div class="container">

                <div class="section-title" data-aos="fade-up">
                    <h2>ข่าวสาร</h2>
                </div>

                <div class="row content">
                    <div class="col-lg-12" data-aos="fade-up" data-aos-delay="450">

                        <ul>
                            <li><i class="ri-check-double-line"></i> แจ้งการทำงานที่ Office/Site ตั้งแต่ 11/5/63 ให้เริ่มเข้าทำตามปกติ ดังนี้</li>
                            <li><i class="ri-check-double-line"></i> ผู้ที่มีรถส่วนตัว ให้เริ่มเข้าทำงานตามปกติที่ Office/Site ตั้งแต่ 11/5/63 เป็นต้นไป ผู้ที่ใช้ขนส่งสาธารณะให้เข้าทำงานตามปกติที่ Office/Site ในวันที่ 11, 13, 15/5/63 และ ตั้งแต่ 18/5/63 เป็นต้นไป</li>
                            <li><i class="ri-check-double-line"></i> หมายเหตุ ช่วงวันที่ 11-15/5/63 ผู้ที่ใช้ขนส่งสาธารณะที่มีข้อจำกัดในการทำงานที่จำเป็นต้องเข้าทุกวัน ก็ให้เข้าทำงานที่ Office/Site ทุกวัน</li>
                        </ul>
                    </div>
                </div>

            </div>
        </section><!-- End About Us Section -->

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-lg-6 text-lg-left text-center">
                    <div class="copyright">
                        &copy; Copyright <strong>PCCTH.COM</strong>. All Rights Reserved
                    </div>
                    <div class="credits">
                        <!-- All the links in the footer should remain intact. -->
                        <!-- You can delete the links only if you purchased the pro version. -->
                        <!-- Licensing information: https://bootstrapmade.com/license/ -->
                        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/vesperr-free-bootstrap-template/ -->
                        Designed by SD1
                    </div>
                </div>
            </div>
        </div>
    </footer><!-- End Footer -->
    </form>

    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{asset('vendor/assets/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/assets/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('vendor/assets/jquery.easing/jquery.easing.min.js')}}"></script>
    <script src="{{asset('vendor/assets/php-email-form/validate.js')}}"></script>
    <script src="{{asset('vendor/assets/waypoints/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('vendor/assets/counterup/counterup.min.js')}}"></script>
    <script src="{{asset('vendor/assets/owl.carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('vendor/assets/isotope-layout/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('vendor/assets/venobox/venobox.min.js')}}"></script>
    <script src="{{asset('vendor/assets/aos/aos.js')}}"></script>

    <!-- Template Main JS File -->
    <script src="{{asset('vendor/assets/js/main.js')}}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

</body>

</html>