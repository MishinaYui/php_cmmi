<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('hpage/home');
});


Route::get('/insert', function () {
    return view('hpage/insertdb');
});


// Route::get('/wi', function () {
//     return view('hpage/wi');
// });

Route::get('/login', function () {
    return view('hpage/login');
});

Route::post('/insertput', 'Controller@insert');
// Route::get('/wi','Controller@selectwi');
Route::get('/um', 'Controller@selectum');
Route::get('/form', 'Controller@selectform');
Route::get('/manage', 'Controller@selectmanage');
Route::get('/delete', 'Controller@destroy');
Route::get('/edit', 'Controller@edidview');
Route::post('/editcode', 'Controller@edicoed');
Route::post('/post-login', 'Controller@login');
Route::get('/ref', 'Controller@selectref');
